#pragma once

#include <JuceHeader.h>
#include "Player/PlayerGUI.h"
#include "Processor/Processor.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent  :  public juce::Component

{
public:
    //==============================================================================
    /** MainComponent constructor */
    MainComponent(Processor& a);

    /** MainComponent destructor */
    ~MainComponent() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    //==============================================================================
    // Your private member variables go here...
    Processor& processor;
    PlayerGUI playerGUI;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
