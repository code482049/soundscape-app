#include "Processor.h"

Processor::Processor()
{
    auto errorMessage = audioDeviceManager.initialiseWithDefaultDevices(1, 2);
    if (!errorMessage.isEmpty())
    { 
        DBG(errorMessage);
    }
    audioDeviceManager.addAudioCallback(this);
}

Processor::~Processor()
{
    audioDeviceManager.removeAudioCallback(this);
}

void Processor::audioDeviceIOCallback(const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples)
{
    //All audio processing is done here
    const float* inL = inputChannelData[0];

    float* outL = outputChannelData[0];
    float* outR = outputChannelData[1];

    while (numSamples--)
    {
        auto output = player.processSample(*inL);
        auto outputR = player.foregroundSoundsRight(*inL);
        auto outputL = player.foregroundSoundsLeft(*inL);
        *outR = output + outputR;
        *outL = output + outputL;

        inL++;
        outR++;
        outL++;
    }
}

void Processor::audioDeviceAboutToStart(juce::AudioIODevice* device)
{
}

void Processor::audioDeviceStopped()
{
}
