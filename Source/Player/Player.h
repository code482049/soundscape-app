#pragma once

#include <JuceHeader.h>
#include <array>

class Player
{
public:
	/** Constructor for the initialisation of Player */
	Player();

	/** Destructor of the Player class */
	~Player();

	/** Starts or stops playback of the Player */
	void setPlayState (bool newState, std::vector <std::vector<bool>> index);

	/** Gets the current playback state */
	bool isPlaying(std::vector <std::vector<bool>> index) const;

	/** Processes the background sounds audio sample by sample */
	float processSample (float input);

	/** Processes the left foreground sounds audio sample by sample */
	float foregroundSoundsLeft(float input);

	/** Processes the right foreground sounds audio sample by sample */
	float foregroundSoundsRight(float input);

	/** Accesses individual samples in the buffer */
	float* getWritePointer(int channelNumber, int sampleOffset);

	/** Method that stores file paths in strings */
	void filePathStorage();

	/** Loads an audio file into the Player */
	void load();

	/** Stores the global gain for background sounds */
	float globalBackgroundSoundsGain = 1.0;

	/** Stores the global gain for foreground sounds */
	float globalForegroundSoundsGain = 1.0;

	/** Keeps tabs on the first background sounds volume */
	std::vector <std::vector <float>> individualBackgroundSoundsGain;

	/** String storage of background sounds file paths */
	std::vector <std::vector<std::string>> backgroundFilePathStorage;

	/** String storage of foreground sounds file paths */
	std::vector <std::string> foregroundFilePathStorage;

	/** Stores the gain values of Background Sounds */
	std::vector <float> bankBackgroundSoundsGain;
	
	/** Stores the gain values of Foreground Sounds */
	std::vector <float> individualForegroundSoundsGain;

	/** Stores pan values for Foreground Sounds */
	std::vector <float> foregroundSoundsPan;

private:
	//Shared data

	/** Indication of on/off states of background sounds */
	std::array <std::array <std::atomic<int>, 6>, 6> playStateBackground{ false };

	/** Indication of on/off states of foreground sounds */
	std::array <std::array <std::atomic<int>, 6>, 2> playStateForeground{ false };

	/** Stores buffer position of background sounds */
	std::vector <std::vector <unsigned int>> bufferPositionBackground { 0 };

	/** Stores buffer position of foreground sounds */
	std::vector <std::vector <unsigned int>> bufferPositionForeground { 0 };

	/** Audio buffer for each of the background sounds */
	std::vector <std::vector <juce::AudioBuffer<float>>> audioBufferBackground;

	/** Audio buffer for each of the foreground sounds (left/right channel)*/
	std::vector <std::vector <juce::AudioBuffer<float>>> audioBufferForeground;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Player)
};
