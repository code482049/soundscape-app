#include "PlayerGUI.h"

PlayerGUI::PlayerGUI()
{
    //MAIN MENU
    for (int i = 0; i < 8; i++)
    {
        addAndMakeVisible(mainMenuBackgroundSounds[i]);
        mainMenuBackgroundSounds[i].addListener(this);

        addAndMakeVisible(mainMenuForegroundSounds[i]);
        mainMenuForegroundSounds[i].addListener(this);
    }

    //Maybe store in string vectors for a more organised code?
    mainMenuBackgroundSounds[0].setButtonText("Soundscapes");
    mainMenuBackgroundSounds[1].setButtonText("Ambient");
    mainMenuBackgroundSounds[2].setButtonText("Rainy Day");
    mainMenuBackgroundSounds[3].setButtonText("Seaside");
    mainMenuBackgroundSounds[4].setButtonText("Drone");
    mainMenuBackgroundSounds[5].setButtonText("Forest");
    mainMenuBackgroundSounds[6].setButtonText("Summer Field");
    mainMenuBackgroundSounds[7].setButtonText("Advanced Settings");

    backgroundSoundsButton[0].setButtonText("Ambient");
    backgroundSoundsButton[1].setButtonText("Rainy Day");
    backgroundSoundsButton[2].setButtonText("Seaside");
    backgroundSoundsButton[3].setButtonText("Drone");
    backgroundSoundsButton[4].setButtonText("Forest");
    backgroundSoundsButton[5].setButtonText("Summer Field");

    mainMenuForegroundSounds[0].setButtonText("Solo Sounds");
    mainMenuForegroundSounds[1].setButtonText("Light Stream");
    mainMenuForegroundSounds[2].setButtonText("River");
    mainMenuForegroundSounds[3].setButtonText("Waterfall");
    mainMenuForegroundSounds[4].setButtonText("Campfire");
    mainMenuForegroundSounds[5].setButtonText("Cat Purr");
    mainMenuForegroundSounds[6].setButtonText("Crows");
    mainMenuForegroundSounds[7].setButtonText("Advanced Settings");

    foregroundSoundsButton[0].setButtonText("Light Stream");
    foregroundSoundsButton[1].setButtonText("River");
    foregroundSoundsButton[2].setButtonText("Waterfall");
    foregroundSoundsButton[3].setButtonText("Campfire");
    foregroundSoundsButton[4].setButtonText("Cat Purr");
    foregroundSoundsButton[5].setButtonText("Crows");

    addAndMakeVisible(backgroundSoundsGlobalVolume);
    backgroundSoundsGlobalVolume.setSliderStyle(juce::Slider::Rotary);
    backgroundSoundsGlobalVolume.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);
    backgroundSoundsGlobalVolume.setRange(0, 100);
    //BGgain.setPopupDisplayEnabled(true, true, nullptr, -1);
    backgroundSoundsGlobalVolume.setValue(100, juce::dontSendNotification);
    backgroundSoundsGlobalVolume.addListener(this);

    addAndMakeVisible(foregroundSoundsGlobalVolume);
    foregroundSoundsGlobalVolume.setSliderStyle(juce::Slider::Rotary);
    foregroundSoundsGlobalVolume.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);
    foregroundSoundsGlobalVolume.setRange(0, 100);
    foregroundSoundsGlobalVolume.setValue(100, juce::dontSendNotification);
    foregroundSoundsGlobalVolume.addListener(this);

    addAndMakeVisible(&backButton);
    backButton.addListener(this);

    //ADVANCED SETTINGS FOR BACKGROUND SOUNDS
    for (int i = 0; i < 6; i++)
    {
        addAndMakeVisible(backgroundSoundsButton[i]);
        backgroundSoundsButton[i].addListener(this);

        addAndMakeVisible(backgroundSoundsIndividualVolume[i]);
        backgroundSoundsIndividualVolume[i].addListener(this);
        backgroundSoundsIndividualVolume[i].setRange(0, 100);
        backgroundSoundsIndividualVolume[i].setSliderStyle(juce::Slider::LinearVertical);

        addAndMakeVisible(backgroundSoundsBankVolume[i]);
        backgroundSoundsBankVolume[i].addListener(this);
        backgroundSoundsBankVolume[i].setRange(0, 100);
        backgroundSoundsBankVolume[i].setValue(50, juce::dontSendNotification);
        backgroundSoundsBankVolume[i].setSliderStyle(juce::Slider::Rotary);
        backgroundSoundsBankVolume[i].setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);

        addAndMakeVisible(backgroundSoundsBankToggle[i]);
        backgroundSoundsBankToggle[i].addListener(this);

        backgroundSounds.push_back(std::vector<float>());
        for (int x = 0; x < 6; x++)
        {
            backgroundSounds[i].push_back(float());
            backgroundSounds[i][x] = 50;
        }

        BGSCheck[i] = false;
    }

    //ADVANCED SETTINGS FOR STATIC SOUNDS
    for (int i = 0; i < 6; i++)
    {
        addAndMakeVisible(foregroundSoundsButton[i]);
        foregroundSoundsButton[i].addListener(this);

        addAndMakeVisible(foregroundSoundsVolume[i]);
        foregroundSoundsVolume[i].addListener(this);
        foregroundSoundsVolume[i].setRange(0, 100);
        foregroundSoundsVolume[i].setValue(50, juce::dontSendNotification);
        foregroundSoundsVolume[i].setSliderStyle(juce::Slider::Rotary);
        foregroundSoundsVolume[i].setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);

        addAndMakeVisible(foregroundSoundsPan[i]);
        foregroundSoundsPan[i].addListener(this);
        foregroundSoundsPan[i].setRange(0, 100);
        foregroundSoundsPan[i].setValue(50, juce::dontSendNotification);
        foregroundSoundsPan[i].setSliderStyle(juce::Slider::Rotary);
        foregroundSoundsPan[i].setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);
        foregroundSoundsPan[i].setValue(50);
    }
}

PlayerGUI::~PlayerGUI()
{
}

void PlayerGUI::resized()
{
    mainMenuBackgroundSounds[0].setBounds(10, 100, 200, 40);
    mainMenuForegroundSounds[0].setBounds(10, 150, 200, 40);
    backgroundSoundsGlobalVolume.setBounds(210, 100, 50, 50);
    foregroundSoundsGlobalVolume.setBounds(210, 150, 50, 50);
}

//SLIDERS
void PlayerGUI::sliderValueChanged(juce::Slider* slider)
{
    for (int i = 0; i < 6; i++)
    {
        if (slider == &backgroundSoundsIndividualVolume[i])
        {
            for (int x = 0; x < 6; x++)
            {
                if (BGSCheck[x] == true)
                {
                    backgroundSounds[x][i] = backgroundSoundsIndividualVolume[i].getValue();
                    player->individualBackgroundSoundsGain[x][i] = cube(backgroundSoundsIndividualVolume[i].getValue());
                }
            }
        }
        if (slider == &backgroundSoundsBankVolume[i])
        {
            player->bankBackgroundSoundsGain[i] = cube(backgroundSoundsBankVolume[i].getValue());
        }
        if (slider == &foregroundSoundsVolume[i])
        {
            player->individualForegroundSoundsGain[i] = cube(foregroundSoundsVolume[i].getValue());
        }
        if (slider == &foregroundSoundsPan[i])
        {
            player->foregroundSoundsPan[i] = foregroundSoundsPan[i].getValue() / 100;
        }
        if (slider == &backgroundSoundsGlobalVolume)
        {
            player->globalBackgroundSoundsGain = cube(backgroundSoundsGlobalVolume.getValue());
        }
        if (slider == &foregroundSoundsGlobalVolume)
        {
            player->globalForegroundSoundsGain = cube(foregroundSoundsGlobalVolume.getValue());
        }
    }
}

float PlayerGUI::cube(float input)
{
    input = input / 100;
    input = input * input * input;
    return input;
}

//BUTTONS
void PlayerGUI::buttonClicked(juce::Button* button)
{
    if (loadCheck == false)
    {
        player->load();
        loadCheck = true;
    }

    if (player == nullptr)
    {
        return;
    }

    if (button == &mainMenuBackgroundSounds[0])
    {
        handleBGsounds();
    }

    std::vector<std::vector<bool>> index;

    //Background and foreground
    for (int a = 0; a < 2; a++)
    {
        index.push_back(std::vector<bool>());
    }
        //Background sounds banks
        for (int b = 0; b < 6; b++)
        {
            //Background individual sounds
            index[0].push_back(bool());
            index[0][b] = false;
        }
        //Foreground sounds left/right channels
        for (int b = 0; b < 6; b++)
        {
            //Foreground individual sounds
            index[1].push_back(bool());
            index[1][b] = false;
        }
    

    for (int i = 0; i < 6; i++)
    {
        int b = 1 + i;
        //BACKGROUND SOUNDS
        if (button == &mainMenuBackgroundSounds[b] || button == &backgroundSoundsBankToggle[i])
        {
            index[0][i] = true;
            player->setPlayState(!player->isPlaying(index), index);
            
            //KEEPING BUTTONS IN SYNC
            mainMenuBackgroundSounds[b].setToggleState(player->isPlaying(index), juce::NotificationType::dontSendNotification);
            backgroundSoundsBankToggle[i].setToggleState(player->isPlaying(index), juce::NotificationType::dontSendNotification);
        }

        //FOREGROUND SOUNDS
        if (button == &mainMenuForegroundSounds[b] || button == &foregroundSoundsButton[i])
        {
            index[1][i] = true;
            player->setPlayState(!player->isPlaying(index), index); //Not all of them are initialised
            //player->setPlayState(!player->isPlaying(index), index);

            //KEEPING BUTTONS IN SYNC
            mainMenuForegroundSounds[b].setToggleState(player->isPlaying(index), juce::NotificationType::dontSendNotification);
            foregroundSoundsButton[i].setToggleState(player->isPlaying(index), juce::NotificationType::dontSendNotification);
        }

    }

    if (button == &mainMenuBackgroundSounds[7])
    {
        advancedBGsounds();
    }

    if (button == &mainMenuForegroundSounds[0])
    {
        handleSsounds();
    }

    if (button == &mainMenuForegroundSounds[7])
    {
        advancedSSsounds();
    }

    if (button == &backButton)
    {
        handleBackButton();
    }

    //BUTTON TOGGLE
    for (int a = 0; a < 6; a++)
    {
        if (button == &backgroundSoundsButton[a])
        {
            for (int b = 0; b < 6; b++)
            {
                BGSCheck[b] = false;
                backgroundSoundsButton[b].setToggleState(false, juce::dontSendNotification);
                backgroundSoundsIndividualVolume[b].setValue(backgroundSounds[a][b]);
            }
            backgroundSoundsButton[a].setToggleState(true, juce::dontSendNotification);
            BGSCheck[a] = true;
        }
    }
}

//MAIN MENU COLLAPSIBLE BUTTONS
void PlayerGUI::handleBGsounds()
{
    int hund = 100;
    if (BGTog == 0)
    {
        mainMenuBackgroundSounds[0].setToggleState(true, juce::dontSendNotification);
        for (int i = 1; i < 8; i++)
        {
            hund = hund + 40;
            mainMenuForegroundSounds[i].setBounds(0, 0, 0, 0);
            mainMenuBackgroundSounds[i].setBounds(10, hund, 200, 40);
            STog = 0;
        }
    }
    else if (BGTog == 1)
    {
        mainMenuBackgroundSounds[0].setToggleState(false, juce::dontSendNotification);
        for (int i = 1; i < 8; i++)
        {
            mainMenuBackgroundSounds[i].setBounds(0, 0, 0, 0);
            BGTog = -1;
        }
    }
    BGTog++;
}

//MAIN MENU COLLAPSIBLE BUTTONS
void PlayerGUI::handleSsounds()
{
    int hund = 150;
    if (STog == 0)
    {
        mainMenuForegroundSounds[0].setToggleState(true, juce::dontSendNotification);
        for (int i = 1; i < 8; i++)
        {
            hund = hund + 40;
            mainMenuBackgroundSounds[i].setBounds(0, 0, 0, 0);
            mainMenuForegroundSounds[i].setBounds(10, hund, 200, 40);
            BGTog = 0;
        }
    }
    else if (STog == 1)
    {
        mainMenuForegroundSounds[0].setToggleState(false, juce::dontSendNotification);
        for (int i = 1; i < 8; i++)
        {
            mainMenuForegroundSounds[i].setBounds(0, 0, 0, 0);
            STog = -1;
        }
    }
    STog++;
}

//ADVANCED BACKGROUND SOUNDS TAB
void PlayerGUI::advancedBGsounds()
{
    //COLLAPSE MAIN MENU
    mainMenuBackgroundSounds[0].setToggleState(false, juce::dontSendNotification);
    backgroundSoundsGlobalVolume.setBounds(0, 0, 0, 0);
    foregroundSoundsGlobalVolume.setBounds(0, 0, 0, 0);
    for (int i = 0; i < 8; i++)
    {
        mainMenuBackgroundSounds[i].setBounds(0, 0, 0, 0);
        mainMenuForegroundSounds[i].setBounds(0, 0, 0, 0);
    }

    //SHOW CONTROLS OF ADVANCED SETTINGS
    backgroundSoundsButton[0].setBounds(10, 10, 145, 100);
    backgroundSoundsButton[1].setBounds(10, 120, 145, 100);
    backgroundSoundsButton[2].setBounds(10, 230, 145, 100);
    backgroundSoundsButton[3].setBounds(165, 10, 145, 100);
    backgroundSoundsButton[4].setBounds(165, 120, 145, 100);
    backgroundSoundsButton[5].setBounds(165, 230, 145, 100);

    backgroundSoundsBankVolume[0].setBounds(10, 10, 50, 50);
    backgroundSoundsBankVolume[1].setBounds(10, 120, 50, 50);
    backgroundSoundsBankVolume[2].setBounds(10, 230, 50, 50);
    backgroundSoundsBankVolume[3].setBounds(165, 10, 50, 50);
    backgroundSoundsBankVolume[4].setBounds(165, 120, 50, 50);
    backgroundSoundsBankVolume[5].setBounds(165, 230, 50, 50);

    backgroundSoundsIndividualVolume[0].setBounds(45, 340, 30, 150);
    backgroundSoundsIndividualVolume[1].setBounds(85, 340, 30, 150);
    backgroundSoundsIndividualVolume[2].setBounds(125, 340, 30, 150);
    backgroundSoundsIndividualVolume[3].setBounds(165, 340, 30, 150);
    backgroundSoundsIndividualVolume[4].setBounds(205, 340, 30, 150);
    backgroundSoundsIndividualVolume[5].setBounds(245, 340, 30, 150);

    backgroundSoundsBankToggle[0].setBounds(130, 85, 25, 25);
    backgroundSoundsBankToggle[1].setBounds(130, 195, 25, 25);
    backgroundSoundsBankToggle[2].setBounds(130, 305, 25, 25);
    backgroundSoundsBankToggle[3].setBounds(285, 85, 25, 25);
    backgroundSoundsBankToggle[4].setBounds(285, 195, 25, 25);
    backgroundSoundsBankToggle[5].setBounds(285, 305, 25, 25);

    backButton.setBounds(10, 500, 50, 30);
}

//ADVANCED STATIC SOUNDS TAB
void PlayerGUI::advancedSSsounds()
{
    //COLLAPSE MAIN MENU
    mainMenuBackgroundSounds[0].setToggleState(false, juce::dontSendNotification);
    backgroundSoundsGlobalVolume.setBounds(0, 0, 0, 0);
    foregroundSoundsGlobalVolume.setBounds(0, 0, 0, 0);
    for (int i = 0; i < 8; i++)
    {
        mainMenuBackgroundSounds[i].setBounds(0, 0, 0, 0);
        mainMenuForegroundSounds[i].setBounds(0, 0, 0, 0);
    }

    //SHOW CONTROLS OF ADVANCED SETTINGS
    foregroundSoundsButton[0].setBounds(10, 10, 145, 100);
    foregroundSoundsButton[1].setBounds(10, 120, 145, 100);
    foregroundSoundsButton[2].setBounds(10, 230, 145, 100);
    foregroundSoundsButton[3].setBounds(165, 10, 145, 100);
    foregroundSoundsButton[4].setBounds(165, 120, 145, 100);
    foregroundSoundsButton[5].setBounds(165, 230, 145, 100);

    foregroundSoundsVolume[0].setBounds(10, 10, 50, 50);
    foregroundSoundsVolume[1].setBounds(10, 120, 50, 50);
    foregroundSoundsVolume[2].setBounds(10, 230, 50, 50);
    foregroundSoundsVolume[3].setBounds(165, 10, 50, 50);
    foregroundSoundsVolume[4].setBounds(165, 120, 50, 50);
    foregroundSoundsVolume[5].setBounds(165, 230, 50, 50);

    foregroundSoundsPan[0].setBounds(10, 60, 50, 50);
    foregroundSoundsPan[1].setBounds(10, 170, 50, 50);
    foregroundSoundsPan[2].setBounds(10, 280, 50, 50);
    foregroundSoundsPan[3].setBounds(165, 60, 50, 50);
    foregroundSoundsPan[4].setBounds(165, 170, 50, 50);
    foregroundSoundsPan[5].setBounds(165, 280, 50, 50);

    backButton.setBounds(10, 500, 50, 30);
}

//BUTTON THAT RETURNS THE USER TO MAIN MENU
void PlayerGUI::handleBackButton()
{
    //REMOVES BACK BUTTON
    backButton.setBounds(0, 0, 0, 0);

    //RESET COMBOBOX
    mainMenuForegroundSounds[0].setToggleState(false, juce::dontSendNotification);
    BGTog = 0;

    //SHOW MAIN MENU
    mainMenuBackgroundSounds[0].setBounds(10, 100, 200, 40);
    mainMenuForegroundSounds[0].setBounds(10, 150, 200, 40);
    backgroundSoundsGlobalVolume.setBounds(210, 100, 50, 50);
    foregroundSoundsGlobalVolume.setBounds(210, 150, 50, 50);

    //COLLAPSE CONTROLS OF ADVANCED SETTINGS
    for (int i = 0; i < 6; i++)
    {
        backgroundSoundsButton[i].setBounds(0, 0, 0, 0);
        backgroundSoundsBankVolume[i].setBounds(0, 0, 0, 0);
        backgroundSoundsIndividualVolume[i].setBounds(0, 0, 0, 0);
        backgroundSoundsBankToggle[i].setBounds(0, 0, 0, 0);

        foregroundSoundsButton[i].setBounds(0, 0, 0, 0);
        foregroundSoundsVolume[i].setBounds(0, 0, 0, 0);
        foregroundSoundsPan[i].setBounds(0, 0, 0, 0);
    }
}

