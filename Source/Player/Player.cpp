#include "Player.h"
#include "../../../../Software Development For Audio/Practical05/JUCE/modules/juce_audio_formats/juce_audio_formats.h"
#define _USE_MATH_DEFINES
#include <math.h>

Player::Player()
{
    //Initialising the file paths into vectors of data
    filePathStorage();

    for (int a = 0; a < 2; a++)
    {
        bufferPositionForeground.push_back(std::vector<unsigned int>());
        audioBufferForeground.push_back(std::vector<juce::AudioBuffer<float>>());
    }

    for (int a = 0; a < 6; a++)
    {
        individualBackgroundSoundsGain.push_back(std::vector<float>());

        bufferPositionBackground.push_back(std::vector<unsigned int>());

        audioBufferBackground.push_back(std::vector<juce::AudioBuffer<float>>());
        for (int b = 0; b < 6; b++)
        {
            individualBackgroundSoundsGain[a].push_back(float());
            individualBackgroundSoundsGain[a][b] = 0.125;

            audioBufferBackground[a].push_back(juce::AudioBuffer<float>());

            //Setting default buffer size
            audioBufferBackground[a][b].setSize(1, 176400);

            //Clearing the contents of the buffer
            audioBufferBackground[a][b].clear();

            //Setting buffer position size
            bufferPositionBackground[a].push_back(unsigned int());

            if (a < 2)
            {
                audioBufferForeground[a].push_back(juce::AudioBuffer<float>());

                //Setting default buffer size
                audioBufferForeground[a][b].setSize(1, 176400);

                //Clearing the contents of the buffer
                audioBufferForeground[a][b].clear();

                bufferPositionForeground[a].push_back(unsigned int());
            }
        }

        //NEEDS MORE ELEGANT OF A SULOUTION
        bankBackgroundSoundsGain.push_back(float());
        bankBackgroundSoundsGain[a] = 0.125;

        individualForegroundSoundsGain.push_back(float());
        individualForegroundSoundsGain[a] = 0.125;

        foregroundSoundsPan.push_back(float());
        foregroundSoundsPan[a] = 0.5;
    }
}

Player::~Player()
{
}

void Player::setPlayState(bool newState, std::vector <std::vector<bool>> index)
{
    //Background sounds
    {
        for (int a = 0; a < 6; a++)
        {
            if (index[0][a] == true)
            {
                for (int b = 0; b < 6; b++)
                {
                    playStateBackground[a][b] = newState;
                }
            }
        }
    }

    //Foreground sounds
    for (int i = 0; i < 6; i++)
    {
        if (index[1][i] == true)
        {
            //Setting left and right channels as newState
            playStateForeground[0][i] = newState;
            playStateForeground[1][i] = newState;
        }
    }
}

bool Player::isPlaying(std::vector <std::vector<bool>> index) const
{
    //Background sounds
    for (int a = 0; a < 6; a++)
    {
        if (index[0][a] == true)
        {
            for (int b = 0; b < 6; b++)
            {
                return playStateBackground[a][b].load();
            }
        }
    }

    //Foreground sounds
    for (int i = 0; i < 6; i++)
    {
        if (index[1][i] == true)
        {
            return playStateForeground[0][i].load();
            return playStateForeground[1][i].load();
        }
    }
}

//PROCESSES THE SAMPLE, LINK WITH PROCESSOR CLASS
float Player::processSample(float input)
{
    std::array <std::array<float*, 6>, 6> audioSample;
    std::vector <float> output;
    float masterOutput = 0.f;

    for (int a = 0; a < 6; a++)
    {
        output.push_back(float());
        for (int b = 0; b < 6; b++)
        {

            if (playStateBackground[a][b] == true)
            {
                audioSample[a][b] = audioBufferBackground[a][b].getWritePointer(0, bufferPositionBackground[a][b]);
                output[a] = output[a] + (*audioSample[a][b] * individualBackgroundSoundsGain[a][b]);
                if (++bufferPositionBackground[a][b] == audioBufferBackground[a][b].getNumSamples())
                {
                    bufferPositionBackground[a][b] = 0;
                }
            }
        }
        masterOutput += (output[a] * bankBackgroundSoundsGain[a]);
    }
    return masterOutput * globalBackgroundSoundsGain;
}

float Player::foregroundSoundsLeft(float input)
{
    std::array <float*, 6> audioSample;
    auto output = 0.f;
    std::vector <float> outTemp;

    for (int i = 0; i < 6; i++)
    {
        outTemp.push_back(float());
        if (playStateForeground[0][i] == true)
        {  
            outTemp[i] = 0;
            audioSample[i] = audioBufferForeground[0][i].getWritePointer(0, bufferPositionForeground[0][i]);
            outTemp[i] += *audioSample[i];
            if (++bufferPositionForeground[0][i] == audioBufferForeground[0][i].getNumSamples())
            {
                bufferPositionForeground[0][i] = 0;
            }
            outTemp[i] *= individualForegroundSoundsGain[i];
            float pan = (1 - foregroundSoundsPan[i]);
            pan = sin(0.5 * M_PI * pan);
            outTemp[i] *= pan;
            output += outTemp[i];
        }
    }
    return output * globalForegroundSoundsGain;
}

float Player::foregroundSoundsRight(float input)
{
    std::array <float*, 6> audioSample;
    auto output = 0.f;
    std::vector <float> outTemp;

    for (int i = 0; i < 6; i++)
    {
        outTemp.push_back(float());
        if (playStateForeground[1][i] == true)
        {
            outTemp[i] = 0;
            audioSample[i] = audioBufferForeground[1][i].getWritePointer(0, bufferPositionForeground[1][i]);
            outTemp[i] += *audioSample[i];
            if (++bufferPositionForeground[1][i] == audioBufferForeground[1][i].getNumSamples())
            {
                bufferPositionForeground[1][i] = 0;
            }
            outTemp[i] *= individualForegroundSoundsGain[i];
            float pan = foregroundSoundsPan[i];
            pan = sin(0.5 * M_PI * pan);
            outTemp[i] *= pan;
            output += outTemp[i];
        }
    }
    return output * globalForegroundSoundsGain;
}

float* Player::getWritePointer(int channelNumber, int sampleOffset)
{
	return nullptr;
}

void Player::load()
{
    //Creating format manager and registering basic formats
    juce::AudioFormatManager formatManager;
    formatManager.registerBasicFormats();

    //2D array of file path variables for background sounds (6 for each bank)
    std::vector <std::vector <juce::File>> backgroundFilePath;

    //2D array of file path variables for foreground sounds (L/R channel)
    std::vector <std::vector <juce::File>> foregroundFilePath;


    //Loading file paths of background noises from string vector
    for (int a = 0; a < 6; a++)
    {
        backgroundFilePath.push_back(std::vector<juce::File>());
        for (int b = 0; b < 6; b++)
        {
            DBG(backgroundFilePathStorage[a][b]);
            backgroundFilePath[a].push_back(juce::File(backgroundFilePathStorage[a][b]));
        }
    }

    //Loading file paths of foreground noises from string vector
    for (int a = 0; a < 2; a++)
    {
        foregroundFilePath.push_back(std::vector<juce::File>());
        for (int b = 0; b < 6; b++)
        {
            foregroundFilePath[a].push_back(juce::File(foregroundFilePathStorage[b]));
        }
    }

    //Background reader pointers
    std::vector<std::vector<std::unique_ptr<juce::AudioFormatReader>>> backgroundReader;
    for (int a = 0; a < 6; a++)
    {
        backgroundReader.push_back(std::vector<std::unique_ptr<juce::AudioFormatReader>>());
        //Creating background sound readers
        for (int b = 0; b < 6; b++)
        {
            backgroundReader[a].push_back(std::unique_ptr<juce::AudioFormatReader>(formatManager.createReaderFor(backgroundFilePath[a][b])));

            //Initialising background sound readers
            if (backgroundReader[a][b] != nullptr)
            {
                audioBufferBackground[a][b].setSize(backgroundReader[a][b]->numChannels, (int)backgroundReader[a][b]->lengthInSamples);
                backgroundReader[a][b]->read(&audioBufferBackground[a][b], 0, (int)backgroundReader[a][b]->lengthInSamples, 0, true, false);
            }
        }
    }

    //Foreground reader pointers
    std::vector<std::vector<std::unique_ptr<juce::AudioFormatReader>>> foregroundReader;
    for (int a = 0; a < 2; a++)
    {
        foregroundReader.push_back(std::vector<std::unique_ptr<juce::AudioFormatReader>>());
        //Creating background sound readers
        for (int b = 0; b < 6; b++)
        {
            foregroundReader[a].push_back(std::unique_ptr<juce::AudioFormatReader>(formatManager.createReaderFor(foregroundFilePath[a][b])));

            //Initialising background sound readers
            if (foregroundReader[a][b] != nullptr)
            {
                //gets overwritten!!!
                audioBufferForeground[a][b].setSize(foregroundReader[a][b]->numChannels, (int)foregroundReader[a][b]->lengthInSamples);
                foregroundReader[a][b]->read(&audioBufferForeground[a][b], 0, (int)foregroundReader[a][b]->lengthInSamples, 0, true, false);
            }
        }
    }
}

void Player::filePathStorage()
{
    //Background sounds - Bank 1
    backgroundFilePathStorage.push_back(std::vector<std::string>());
    backgroundFilePathStorage[0].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Ambient/1. Bass.mp3");
    backgroundFilePathStorage[0].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Ambient/2. Guitar.mp3");
    backgroundFilePathStorage[0].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Ambient/3. Male Choir.mp3");
    backgroundFilePathStorage[0].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Ambient/4. Female Choir.mp3");
    backgroundFilePathStorage[0].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Ambient/5. Pluck Synth.mp3");
    backgroundFilePathStorage[0].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Ambient/6. Piano.mp3");

    //Background sounds - Bank 2
    backgroundFilePathStorage.push_back(std::vector<std::string>());
    backgroundFilePathStorage[1].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Storm/1. Light Rain.mp3");
    backgroundFilePathStorage[1].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Storm/2. Breeze.mp3");
    backgroundFilePathStorage[1].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Storm/3. Distant Thunder.mp3");
    backgroundFilePathStorage[1].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Storm/4. Heavy Rain.mp3");
    backgroundFilePathStorage[1].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Storm/5. Cold Wind.mp3");
    backgroundFilePathStorage[1].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Storm/6. Close Thunder.mp3");

    //Background sounds - Bank 3
    backgroundFilePathStorage.push_back(std::vector<std::string>());
    backgroundFilePathStorage[2].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Beach/1. Wind.mp3");
    backgroundFilePathStorage[2].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Beach/2. Distant Waves.mp3");
    backgroundFilePathStorage[2].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Beach/3. Pebble Waves.mp3");
    backgroundFilePathStorage[2].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Beach/4. Gentle Waves.mp3");
    backgroundFilePathStorage[2].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Beach/5. Ship Creak.mp3");
    backgroundFilePathStorage[2].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Beach/6. Seagulls.mp3");

    //Background sounds - Bank 4
    backgroundFilePathStorage.push_back(std::vector<std::string>());
    backgroundFilePathStorage[3].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Drone/6. Synth.mp3");
    backgroundFilePathStorage[3].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Drone/1. Guitar Feedback.mp3");
    backgroundFilePathStorage[3].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Drone/4. Blender.mp3");
    backgroundFilePathStorage[3].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Drone/5. Bottle.mp3");
    backgroundFilePathStorage[3].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Drone/2. Oven.mp3");
    backgroundFilePathStorage[3].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Drone/3. White Noise.mp3");

    //Background sounds - Bank 5
    backgroundFilePathStorage.push_back(std::vector<std::string>());
    backgroundFilePathStorage[4].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Forest/1. Leaves.mp3");
    backgroundFilePathStorage[4].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Forest/2. Leaves.mp3");
    backgroundFilePathStorage[4].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Forest/3. Birdsong.mp3");
    backgroundFilePathStorage[4].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Forest/4. Birdsong.mp3");
    backgroundFilePathStorage[4].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Forest/5. Birdsong.mp3");
    backgroundFilePathStorage[4].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Forest/6. Bees.mp3");

    //Background sounds - Bank 6
    backgroundFilePathStorage.push_back(std::vector<std::string>());
    backgroundFilePathStorage[5].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Field/1. Grass Rustle.mp3");
    backgroundFilePathStorage[5].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Field/2. Summer Wind.mp3");
    backgroundFilePathStorage[5].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Field/3. Crickets 1.mp3");
    backgroundFilePathStorage[5].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Field/4. Crickets 2.mp3");
    backgroundFilePathStorage[5].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Field/5. Cicadas.mp3");
    backgroundFilePathStorage[5].push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/Soundscapes/Field/6. Owl.mp3");

    //Foreground sounds
    foregroundFilePathStorage.push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/1. Light Stream.mp3");
    foregroundFilePathStorage.push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/2. Stream.mp3");
    foregroundFilePathStorage.push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/3. Waterfall.mp3");
    foregroundFilePathStorage.push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/4. Campfire.mp3");
    foregroundFilePathStorage.push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/5. Cat Purr.mp3");
    foregroundFilePathStorage.push_back("C:/Users/Ajo/Documents/Creative Music Technology/Final Year Project/ASRA/Resources/6. Crows.mp3");
}
