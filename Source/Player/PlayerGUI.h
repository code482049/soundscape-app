#pragma once

#include <JuceHeader.h>
#include "Player.h"

class PlayerGUI :	public juce::Component,
					public juce::Button::Listener,
					public juce::Slider::Listener
{
public:
	/** Constructor for the PlayerGUI class */
	PlayerGUI();

	/** Destructor for the PlayerGUI class */
	~PlayerGUI();

	/** Sets the Player for this GUI object to control */
	void setPlayer(Player* playerPtr) { player = playerPtr; }

	/** Resized function from the Component class*/
	void resized() override;

	/** Listens for button callbacks */
	void buttonClicked(juce::Button* button) override;

	/** Listens for slider callbacks */
	void sliderValueChanged(juce::Slider* slider) override;

	/** Cubes the gain */
	float cube(float input);

	/** Handles background sound combobox */
	void handleBGsounds();

	/** Handles static sound combobox */
	void handleSsounds();

	/** Handles Advanced Settings GUI for Background Sounds */
	void advancedBGsounds();

	/** Handles Advanced Settings GUI for Static Sounds */
	void advancedSSsounds();

	/** Handles Back button */
	void handleBackButton();

private:
	//THESE VARIABLES NEED MORE COMPREHENSIVE NAMING CONVENTION

	//Pointer to the player object
	Player* player{ nullptr };

	//Checks if load function has already been executed
	bool loadCheck = false;

	//Declaration of Play button
	juce::TextButton backButton { "Back" };

	//Declaration of makeshift multi-choice combo-box
	std::array <juce::TextButton, 8> mainMenuBackgroundSounds;
	std::array <juce::TextButton, 8> mainMenuForegroundSounds;

	//Advanced Settings BS components
	std::array <juce::TextButton, 6> backgroundSoundsButton;
	std::array <juce::Slider, 6> backgroundSoundsIndividualVolume;
	std::array <juce::Slider, 6> backgroundSoundsBankVolume;
	std::array <juce::ToggleButton, 6> backgroundSoundsBankToggle;

	//Advanced Settings SS components
	std::array <juce::TextButton, 6> foregroundSoundsButton;
	std::array <juce::Slider, 6> foregroundSoundsVolume;
	std::array <juce::Slider, 6> foregroundSoundsPan;

	int BGTog = 0;
	int STog = 0;

	//Global volume sliders
	juce::Slider backgroundSoundsGlobalVolume;
	juce::Slider foregroundSoundsGlobalVolume;

	//Background Sounds
	std::vector <std::vector <float>> backgroundSounds;

	std::array <bool, 6> BGSCheck;
};
